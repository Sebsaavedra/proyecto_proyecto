# -*- coding: utf-8 -*-
from odoo import models, fields, api


class proyecto_daf(models.Model):
    _name = 'proyecto.proyecto'
    _inherit = ['mail.thread', 'mail.activity.mixin', 'portal.mixin']
    _description = "Proyecto DAF"
    _rec_name = "nombre_proyecto"



    nombre_proyecto = fields.Char(string="Nombre Proyecto",track_visibility='onchange', index=True, required=True)
    codigo_proyecto = fields.Char(string="Codigo Proyecto",track_visibility='onchange', index=True, required=True)




    message_attachment_count = fields.Integer(string="Conteo de archivos adjuntos", track_visibility='onchange', index=True)
    message_channel_ids = fields.Many2many('mail.channel', string="Seguidores (Canales)", track_visibility='onchange', index=True)
    message_follower_ids = fields.One2many('mail.followers', 'res_id', track_visibility='onchange', index=True)
    message_ids = fields.One2many('mail.message', 'res_id', track_visibility='onchange', index=True)
    message_main_attachment_id = fields.Many2one('ir.attachment', track_visibility='onchange', index=True)

#     name = fields.Char()
#     value = fields.Integer()
#     value2 = fields.Float(compute="_value_pc", store=True)
#     description = fields.Text()

#     @api.depends('value')
#     def _value_pc(self):
#         self.value2 = float(self.value) / 100




